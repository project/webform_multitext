/**
 * JS for Multitext field for webform from @jentre, TGPO Consult, Ltd.
 */

(function( $ ) {
	
	function input_changed() {
		var div = $(this).closest('div.multitext-display');
		var initial = $(this).closest('.webform-component-multitext').find('input.form-text');
		if (div.length > 0 && initial.length > 0) {
			values = [];
			div.find('input.multitext-form-text').each(function(){
				if ($(this).val().length > 0) {
					values.push($(this).val());
				}
			});
			initial.val(JSON.stringify(values));
		}
	}
	
	function remove_field() {
		var ul_container = $(this).parent().parent();
		if (ul_container.children().length > 1) {
			$(this).parent().remove();
		} else {	// The last one we blank instead of removing
			var element = ul_container.find('.multitext-form-text'); 
			element.val('');
		}
	}
	
	function add_button() {
		var ul_container = $(this).parent().children().first();
		var new_li = ul_container.children().last().clone();
		// Blank out the values in this clone
		new_li.children().each(function(i) {
			if ($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button') {
				$(this).val('');
			}
		});
		ul_container.append(new_li);
		$(".multitext-remove-button").on('click', remove_field);
	}
	
	Drupal.behaviors.traceability = {
			attach: function (context, settings) {
				// Setup the initial display fields and hide the actual field
				$('.webform-component-multitext').each(function(){
					var initial = $(this).find('input.form-text');
					initial.hide();	// Hide the actual input
					var size = initial.prop('size');
					var values;
					if (initial.val().length && initial.val() != '[]') {
						values = JSON.parse(initial.val());	// Initial values
					} else {
						values = [''];	// One blank option to start the list
					}
					var newdiv = '<div class="multitext-display"><ul>';
					for (n = 0; n < values.length; ++n) {
						newdiv += "<li><input type='text' class='multitext-form-text' name='multitext[]' size='" + size 
							+ "' value='" + values[n] 
							+ "' />&nbsp;<input type='button' class='multitext-remove-button' value='x' /></li>";
					}
					newdiv += '</ul><br/><input type="button" value="' + Drupal.settings.webform_multitext.add_text + '" class="multitext-add-button" /></div>';
					$(this).append(newdiv);
				})
				
				$('input.multitext-form-text').blur(input_changed);
				$(".multitext-add-button").click(add_button);
				$(".multitext-remove-button").click(remove_field);
				$(".webform-submit").click(function() {
					$(this).parent().parent().find('input.multitext-form-text').trigger('blur');
				});
			}
	}
	
})( jQuery );