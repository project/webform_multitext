
Module: Webform multitext
Author: Jennifer Trelewicz <http://drupal.org/user/1601538> <http://tgpo.ru>


Description
===========
Provides a true multiple-value text field for webforms. Values are saved in the database as one JSON-encoded
array, since webform does not easily support deltas.

Requirements
============
Webform

Installation
============
Copy the 'webform_multitext' module directory in to your Drupal sites/all/modules directory as usual.

Usage
=====
Like any other webform component plugin - you will see it in the component types list and can use/disable
per the API as all others.
